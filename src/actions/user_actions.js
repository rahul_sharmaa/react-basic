import * as API from './../base/api_service';
import * as serviceCallConfig from './../config/api_urls';


export var USER_LOGIN = 'USER_LOGIN';
export var USER_LOGIN_SUCCESS = 'USER_LOGIN_SUCCESS';
export var USER_LOGIN_FAILED = 'USER_LOGIN_FAILED';

export var userLogin = (payload) => {
    return (dispatch, getState) => {
        dispatch({
            type: USER_LOGIN
        })
        API.makeServiceCall(
            serviceCallConfig.API_POST,
            dispatch,
            serviceCallConfig.get(serviceCallConfig.LOGIN),
            payload,
            serviceCallConfig.LOGIN,
            false,
            ''
        );
    };
};

export var userLoginSuccess = (response) => {
    return (dispatch, getState) => {
        dispatch({
            type: USER_LOGIN_SUCCESS,
            data: response
        })
    };
};

export var userLoginFailed = (response) => {
    return (dispatch, getState) => {
        dispatch({
            type: USER_LOGIN_FAILED,
            data: response
        })
    };
};



