

import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory } from 'react-router';
import createExpirationTransform from 'redux-persist-transform-expire';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { persistStore, autoRehydrate } from 'redux-persist';
import asyncLocalStoragefrom from 'redux-persist/storages';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';
import './css/app.css';
import AppReducer from './reducers';


/** Screen components */
import * as UserActions from './actions/user_actions';
import Home from './screens/home';
import Login from './screens/login';
import Register from './screens/register';

let reduxLogger = createLogger();
var startTime = new Date().getTime();
var timeOutTime = startTime + (44 * 60 * 1000);
var previousInterVal;
let store = createStore(AppReducer,
    window.devToolsExtension ? window.devToolsExtension() : f => f,
    process.env.NODE_ENV === 'production'
        ? applyMiddleware(thunk)
        : applyMiddleware(thunk, reduxLogger),
    autoRehydrate()

);
handleSession(timeOutTime);
let reduxInitTime = new Date();
const expireTransform = createExpirationTransform({
    expireKey: reduxInitTime.setHours(reduxInitTime.getHours() + 2)
});

// if you don't want to use browser history
// createMemoryHistory from react-router
// let history = createMemoryHistory(location);
let persistor = persistStore(store,
    {
        storage: asyncLocalStoragefrom,
        transforms: [expireTransform]
    },
    (err, prevSate) => {
        if (err) {
            console.log(err);
        }
        if ('serviceWorker' in navigator) {
            navigator.serviceWorker.register('service-worker.js')
                .then((reg) => {
                    console.log(reg);
                })
                .catch((e) => {
                    console.log(e);
                });
        }
        ReactDOM.render((
            <Provider store={store} persistor={persistor}>
                <Router history={browserHistory}>
                    <Route path='/' component={Register} onEnter={onRouteEnter}>
                        <Route path='/Home' component={Home} onEnter={onRouteEnter} />
                    </Route>
                </Router>
            </Provider >
        ), document.getElementById('root'));
    });

window.onerror = (message, source, lineno, colno, error) => {
    console.log(message, source, lineno, colno, error);
};

/*global ga*/

function onRouteEnter({routes, params, location}, replaceFunc) {
  // function to be called when route is entered or the page is rendererd
  console.info('On Route Enter Called.....');
}


function handleSession(endTime) {
    if (previousInterVal) {
        clearInterval(previousInterVal);
    }
    setInterval(function () {
        var currentTime = new Date().getTime();
        if (currentTime >= endTime) {
            localStorage.clear();
            window.location.reload();
        }
    }, 1000)
}
