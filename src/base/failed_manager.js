import * as api_urls from './../config/api_urls';
import * as UserActions from './../actions/user_actions';

export var failed_manager =
    (dispatch, endpoint, payload, response, error) => {
        if (response && response.message === 'User is offline.') {
            alert('Please check your internet connection and try again.');
        } else if (!response && endpoint !== api_urls.AUTH_KEY) {
            if (response !== false) {
                console.log('Invalid API response received for URL : ' + endpoint);
                console.log(error);
                // alert('Unexpected error occurred. Please try again.');
            }
        }

        if (response && response.data && response.data.msg && response.data.msg === 'Authentication Timeout') {
            response.data.msg = 'Creating your new session.';
        }
        switch (endpoint) {
            case api_urls.LOGIN: {
                return dispatch(UserActions.userLoginFailed(response));
            };
                break;
            default: {
                alert('API Service Failed..')
            }
        }
    };
