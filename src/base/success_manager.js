import * as api_urls from './../config/api_urls';
import * as UserActions from './../actions/user_actions';


export var success_manager = (dispatch, endpoint, payload, response) => {
    switch (endpoint) {

        case api_urls.LOGIN:
            {
                return dispatch(UserActions.userLoginSuccess(response));
            };
            break;
        default:
            {
                if (response) {
                    return response;
                } else {
                    return { status: 200, message: 'No data.' };
                }
            }
    }
};
