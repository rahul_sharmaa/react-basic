// ------ Declare Constants Here --------//
export var BASE_URL = 'http://localhost:82/helloepos/api/public/api'
export var API_KEY='';
export var API_GET = 'GET';
export var API_POST = 'POST';

//-------  DECLARE ENDPOINTS HERE  --------//
export var LOGIN = '/auth/login' ;
export var FETCH_USERS = '/users/all' ;

//--------- RETURN FINAL URLS -------------//
export var get = (url, data) => {
    switch (url) {
        case LOGIN:{
            return  BASE_URL + LOGIN;
        }
        case FETCH_USERS:{
            return  BASE_URL + FETCH_USERS;
        }
        default:
            break;
    }
    return 'URL Not configured';
};
