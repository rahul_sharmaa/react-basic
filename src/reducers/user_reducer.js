import * as UserActions from './../actions/user_actions';
import extend from 'lodash/extend';


export var User = (state = {
    IsLoggedIn: false,
    IsRegisterd: false,
    userDetails: {}
}, action) => {
    switch (action.type) {
        // // Uncomment below block only if you want to persist store data
        case 'persist/REHYDRATE': {
            if (action.payload.User) {
                action.payload.User.IsLoggingIn = false;
            } else {
                return state;
            }
        }

        case UserActions.USER_LOGIN:
            {
                return extend({}, state, {});
            }

        case UserActions.USER_LOGIN_SUCCESS:
            {
                return extend({}, state, { userDetails: action.data });
            }
        case UserActions.USER_LOGIN_FAILED:
            {
                return extend({}, state, { userDetails: action.data });
            }
        default:
            return state;
    }
};
