import { User } from './user_reducer';
import { combineReducers } from 'redux';


let AppReducer = combineReducers({
    User
});
export default AppReducer;

