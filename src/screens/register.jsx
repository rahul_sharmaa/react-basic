import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { bindActionCreators } from 'redux';

let registerScreen = React.createClass({
  componentWillMount(){},
  componentDidMount(){},
  componentWillReceiveProps(){},
  render(){
    return(
      <div>
        <h3>Registeration Form</h3>
        <table>
          <tr>
            <td>Name</td>
            <td><input type="text" ref="name"/></td>
          </tr>
          <tr>
            <td>age</td>
            <td><input type="number" ref="age" min={1} max={100}/></td>
          </tr>
          <tr>
            <td></td>
            <td><input type="submit" value="Submit"/></td>
          </tr>
        </table>
      </div>
    )
  }
})

let mapStateToProps = (state) => {
  return {
  };
};

let mapDispatchToProps = (dispatch) => ({
});

export default connect(mapStateToProps,
  mapDispatchToProps)(withRouter(registerScreen));