import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
let screenIdentifier = 'deiveryPickupScreen';
import * as UserActions from './../actions/user_actions';
import { bindActionCreators } from 'redux';

let PickupDelivery = React.createClass({
    getInitialState() {
        return {
             someState:'SomeValue'
        }
    },
    componentWillReceiveProps(nextProps) {

    },
    componentDidMount() {
       
    },
    loginHandler(){
         this.props.dispatch(UserActions.userLlogin());
    },
    navigationHandler(){
    },
    render() {
        return (
            <div className='home-page-container'>
                <h3>Home Page</h3>
               <div className='btn-holder'><button onClick={this.loginHandler}>Login</button></div>
            </div>
        );
    }
});

let mapStateToProps = (state) => {
    return {
    
    };
};

let mapDispatchToProps = (dispatch) => ({
    UserActions: bindActionCreators(UserActions, dispatch),
    dispatch: dispatch
});
export default connect(mapStateToProps,
    mapDispatchToProps)(withRouter(PickupDelivery));

