import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { bindActionCreators } from 'redux';
import * as UserActions from './../actions/user_actions';

let loginScreen = React.createClass({

  componentWillReceiveProps(nextProps){
    if(nextProps.userDetails && nextProps.userDetails.message) {
      this.setState({
        userDetails: nextProps.userDetails.message
      })
    }
  },
  componentWillMount(){
    this.setState({
      emailError: false,
      passwordError: false,
      userDetails: false
    })
  },
  login(){
    let email = this.refs.fldEmail.value;
    let password = this.refs.fldPassword.value;
    let error = false;
    if(email.trim() === "") {
      error = true;
      this.setState({
        emailError: "Email cannot be left blank",
      })
    }
    if(password.trim() === "") {
      error = true;
      this.setState({
        passwordError: "Password cannot be left blank",
      })
    }
    /** Starting Point */
    if(!error) {
      /** Payload */
      let payload = {email: email, password: password};
      this.props.dispatch(UserActions.userLogin(payload))
    }
  },
  setEmail(event){
    if(event.target.value.trim() === "") {
      this.setState({
        emailError: "Email cannot be left blank",
      })      
    } else {
      this.setState({
        emailError: false,
      })      
    }
  },
  render(){
    return (
      <div>
        <table>
          <tbody>
            <tr>
              <td>Email</td>
              <td><input type="text" ref="fldEmail" onChange={this.setEmail} /></td>
            </tr>
            {this.state.emailError && <tr>
              <td></td>
              <td>{this.state.emailError}</td>
            </tr>}
            <tr>
              <td>Password</td>
              <td><input type="password" ref="fldPassword" /></td>
            </tr>
            {this.state.passwordError && <tr>
              <td></td>
              <td>{this.state.passwordError}</td>
            </tr>}
            <tr>
              <td></td>
              <td><input type="submit" value="Login" onClick={this.login} /></td>
            </tr>
          </tbody>
        </table>
        {this.state.userDetails && <tr>
              <td></td>
              <td>{this.state.userDetails}</td>
            </tr>}
      </div>
    );
  }
});

let mapStateToProps = (state) => {
  return {
    userDetails: state.User.userDetails
  };
};

let mapDispatchToProps = (dispatch) => ({
    dispatch: dispatch
});

export default connect(mapStateToProps,
  mapDispatchToProps)(withRouter(loginScreen));