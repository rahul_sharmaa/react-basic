## Redux_React Mobile Web App


## Prerequisites
    * Node - v6.6.0
    * NPM - v3.10.8

## Install Packages
    * Run command `npm install` on the project directory 
    to install equired packages.    


## Environments

    Development:
    1. Run command `npm start`.
    2. Open http://localhost:3000/ in browser.

    Production:
    1. Run command `npm run build`.
    2. Output will be copied to `./build` folder. 


## Service Worker

    We would need a service worker file and below command create the same.
    * gulp generate-service-worker-dev
